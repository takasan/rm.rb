#!/usr/bin/env ruby

require 'fileutils'
require 'date'

TRASH = "#{ENV['HOME']}/.trash"

def main(argv)
  argv.each do |p|
    if File.directory?(p) or File.file?(p) then
      trash(p)
    elsif
      puts "#{p}: No such file or directory"
    end
  end
end

def trash(path)
  trash_path = "#{TRASH}/#{File.basename(path)}"
  if File.exists?(trash_path) then
    dirname =  File.dirname(File.expand_path(path)).gsub("/",'!')
    date = DateTime.now.strftime("%Y:%m:%d-%H:%M")
    trash_path += "-" + dirname + "-" + date
  end

  FileUtils.move(path, trash_path)
end

if ARGV.size > 0 then
  main(ARGV)
else
  puts 'Usage: rm file...'
end

